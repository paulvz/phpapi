import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor() { }

  countries = [{
      'name': 'Botswana',
      'code': '+267'
    },
    {
      'name': 'Lesoto',
      'code': '+266'
    },
    {
      'name': 'South Africa',
      'code': '+27'
    },
    {
      'name': 'Mozambique',
      'code': '+258'
    },
    {
      'name': 'Namibia',
      'code': '+264'
    },
  ];
}
