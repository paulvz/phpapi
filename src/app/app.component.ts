import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ApiService } from './services/api.service';
import { CountryService } from './services/country.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  countries = {};
  code = '';

  registerForm: FormGroup;
  submitted = false;

  constructor(
    private countryService: CountryService,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
  ) { }

  getCountryCode(e) {
    for (const c in this.countryService.countries) {
      if (this.countryService.countries[c].name === e) {
        this.code = this.countryService.countries[c].code;
      }
    }
    if (e === '') {
      this.code = '';
    }
  }

  ngOnInit() {
    this.countries = this.countryService.countries;

    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(20)]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(8)]],
      email:    ['', [Validators.required, Validators.email]],
      country:  ['', Validators.required],
      code:     ['', Validators.required],
      phone:    ['', Validators.required],
      terms:    ['', Validators.required],
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    const formData = this.registerForm.value;
    const formValue = {
      username: formData.username,
      password: formData.password,
      email:    formData.email,
      country:  formData.country,
      phone:    formData.code + '' + formData.phone,
    };

    this.apiService.createUser(formValue).subscribe((response) => {
      console.log(response);
      if (response.message === 'Success') {
        alert(response.message);
      } else {
        alert('Error\n' + response.message);
      }
    });
  }
}
