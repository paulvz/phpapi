<?php
class Validation {
    private $connection;

    public function __construct($connection){
        $this->connection = $connection;
    }

    public function username($fld) {
        if (trim($fld) == '')
            return "Usern may not be empty";

        // test if exists in DB
        $query = "SELECT username FROM tblusers WHERE username = '".$fld."'";

        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        $cnt = $stmt->rowCount();
        if ($cnt > 0)
            return "Username exists";

        $len = strlen($fld);
        if ($len < 5)
            return "Username is too short, minimum is 5 characters (20 max)";
        elseif ($len > 20)
            return "Username is too long, maximum is 20 characters (5 min)";

        // test id valid username
        if (!preg_match("/^[a-zA-Z0-9]*$/", $fld))
            return "Only letters and numbes allowed";
    }

    public function password($fld) {
        if (trim($fld) == '')
            return "Password may not be empty";

        $max = 8;
        $min = 6;
        $len = strlen($fld);
        if ($len < $min)
            return "Password is too short, minimum is $min characters ($max max)";
        elseif ($len > $max)
            return "Password is too long, maximum is $max characters ($min min)";
    }

    public function email($fld) {
        if (trim($fld) == '')
            return "Email may not be empty";

        if (!filter_var($fld, FILTER_VALIDATE_EMAIL))
          return "Invalid email format"; 
    }

    public function country($fld) {
        if (trim($fld) == '')
            return "Country may not be empty";
    }

    public function phone($fld) {
        if (trim($fld) == '')
            return "Phone may not be empty";

        if (!preg_match('/^(?!(?:\d*-){5,})(?!(?:\d* ){5,})\+?[\d- ]+$/', $fld))
            return "Invalid phone format";
    }
}