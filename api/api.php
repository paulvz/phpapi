<?php
class Api {
    private $connection;

    public function __construct($connection){
        $this->connection = $connection;
    }

    public function create($tblName, $data) {
        $data->password = password_hash($data->password, PASSWORD_DEFAULT);
        $query = "INSERT INTO " . $tblName . " VALUES (NULL, '". implode("','", (array) $data)."')";

        $stmt = $this->connection->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    public function read($tblName, $where = '') {
        if (!empty($where)) {
            reset($where);
            $where = "WHERE '".key($where)."' = '".current($where)."'";
        }

        $query = "SELECT * FROM " . $tblName . " ". $where;

        $stmt = $this->connection->prepare($query);
        $stmt->execute();

        return $stmt;
    }


}