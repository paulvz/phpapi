<?php
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

header("Access-Control-Allow-Origin: *");

require_once './db.php';
require_once './api.php';
require_once './validation.php';

$db = new Db();
$con = $db->getConnection();
$api = new Api($con);
$validation = new Validation($con);

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $stmt = $api->read($_GET['tbl']);
    $cnt = $stmt->rowCount();

    if ($cnt > 0) {
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        echo json_encode(
            array("body" => $rows, "count" => $cnt)
        );
    } else {
        echo json_encode(
            array("body" => array(), "count" => 0)
        );
    }
} elseif ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
} elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $res = [];
    $data = json_decode(file_get_contents("php://input"));

    if ($msg = $validation->username($data->username))
        $res['message'] = $msg;
    if ($msg = $validation->password($data->password))
        $res['message'] = $msg;
    if ($msg = $validation->email($data->email))
        $res['message'] = $msg;
    if ($msg = $validation->country($data->country))
        $res['message'] = $msg;
    if ($msg = $validation->phone($data->phone))
        $res['message'] = $msg;

    if (empty($res)) {
        $response = $api->create(/*$_GET['tbl']*/'tblusers', $data);
        $res['message'] = "Success";
    }

    echo json_encode($res);
}